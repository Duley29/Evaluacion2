﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacionn.Models
{
    public class Instructor
    {

        [Key]
        public int InstructorID { get; set; }


        [StringLength(30, MinimumLength = 3, ErrorMessage = "El nombre de la vinculacion ingresado no tiene la cantidad necesaria de caracteres(minimo:1 - maximo:25)")]
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string Nombre { get; set; }

        [StringLength(30, MinimumLength = 3, ErrorMessage = "El nombre de la vinculacion ingresado no tiene la cantidad necesaria de caracteres(minimo:1 - maximo:25)")]
        [Display(Name = "Apellido")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string Apellido { get; set; }


        [Required(ErrorMessage = "Email es obligatorio")]
        [RegularExpression("^[_a-z0-9-]+(.[_a-z0-9-]+)@(hotmail.com|outlook.com|gmail.com)$", ErrorMessage = "Correo invalido")]
        [DataType(DataType.EmailAddress)]


        [Display(Name = "Correo Electronico")]
        public string Email { get; set; }


        [Required]
        [DataType(DataType.Date)]
    [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",ApplyFormatInEditMode = true)]
        public DateTime DueDate { get; set; }

        public int EspecialidadID { get; set; }
        public int VinculacionID { get; set; }


        public virtual Especialidad Especialidad { get; set; }
        public virtual Vinculacion Vinculacion { get; set; }
    }
}
