﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Evaluacionn.Models
{
  
   
        public class EvaluacionContext : DbContext

        {
            public EvaluacionContext() : base("DefaultConnection")
            {

            }
            protected override void Dispose(bool disposing)
            {
                base.Dispose(disposing);
            }

        public System.Data.Entity.DbSet<Evaluacionn.Models.Especialidad> Especialidads { get; set; }

        public System.Data.Entity.DbSet<Evaluacionn.Models.Vinculacion> Vinculacions { get; set; }

        public System.Data.Entity.DbSet<Evaluacionn.Models.Instructor> Instructors { get; set; }
    }
}