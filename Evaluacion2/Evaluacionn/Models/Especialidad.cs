﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacionn.Models
{
    public class Especialidad
    {

        [Key]
        public int EspecialidadID { get; set; }


        [Display(Name = "Nombre de especialidad")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string name_specialidad { get; set; }


        [Display(Name = "Estado")]
        public Boolean status { get; set; }
    
}
}