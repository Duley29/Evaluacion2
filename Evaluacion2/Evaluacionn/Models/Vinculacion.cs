﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacionn.Models
{
    public class Vinculacion
    {
        [Key]
        public int VinculacionID { get; set; }



        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "vinculacion")]
        public string name_vinculacion { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Horas")]
        public int hours { get; set; }



        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Estado")]
        public Boolean status { get; set; }
    }
}
