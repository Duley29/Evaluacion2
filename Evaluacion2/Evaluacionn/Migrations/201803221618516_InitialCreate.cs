namespace Evaluacionn.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Especialidads",
                c => new
                    {
                        EspecialidadID = c.Int(nullable: false, identity: true),
                        name_specialidad = c.String(nullable: false),
                        status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.EspecialidadID);
            
            CreateTable(
                "dbo.Instructors",
                c => new
                    {
                        InstructorID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 30),
                        Apellido = c.String(nullable: false, maxLength: 30),
                        Email = c.String(nullable: false),
                        DueDate = c.DateTime(nullable: false),
                        EspecialidadID = c.Int(nullable: false),
                        VinculacionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InstructorID)
                .ForeignKey("dbo.Especialidads", t => t.EspecialidadID, cascadeDelete: true)
                .ForeignKey("dbo.Vinculacions", t => t.VinculacionID, cascadeDelete: true)
                .Index(t => t.EspecialidadID)
                .Index(t => t.VinculacionID);
            
            CreateTable(
                "dbo.Vinculacions",
                c => new
                    {
                        VinculacionID = c.Int(nullable: false, identity: true),
                        name_vinculacion = c.String(nullable: false),
                        hours = c.Int(nullable: false),
                        status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.VinculacionID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Instructors", "VinculacionID", "dbo.Vinculacions");
            DropForeignKey("dbo.Instructors", "EspecialidadID", "dbo.Especialidads");
            DropIndex("dbo.Instructors", new[] { "VinculacionID" });
            DropIndex("dbo.Instructors", new[] { "EspecialidadID" });
            DropTable("dbo.Vinculacions");
            DropTable("dbo.Instructors");
            DropTable("dbo.Especialidads");
        }
    }
}
