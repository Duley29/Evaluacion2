﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Evaluacionn.Startup))]
namespace Evaluacionn
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
